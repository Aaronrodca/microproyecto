using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GirarTrampa : MonoBehaviour
{
    public float amplitude = 20f;
    public float speed = 2f;
    private Quaternion initialRot;

    // Start is called before the first frame update
    void Start()
    {
        initialRot = transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        float angle = amplitude * Mathf.Sin(Time.time * speed);
        transform.rotation = initialRot * Quaternion.Euler(0, 0, angle);
    }
}
