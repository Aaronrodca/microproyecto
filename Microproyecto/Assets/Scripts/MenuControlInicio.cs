using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuControlInicio : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        // Comprueba si el siguiente �ndice de escena es v�lido
        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            Debug.Log("No hay m�s niveles para cargar.");
            // Aqu� puedes reiniciar el juego o ir a una pantalla de fin de juego
            // SceneManager.LoadScene(0); // Ejemplo de reiniciar el juego
        }
    }

    public void SalirDelJuego()
    {
        // Salir de la aplicaci�n
        Application.Quit();
    }
}
