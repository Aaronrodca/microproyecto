using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviur : MonoBehaviour
{
    [SerializeField] float moveSpeed = 1f;
    Rigidbody2D myRigibody;
    BoxCollider2D myBoxCollider;
    void Start()
    {
        myRigibody = GetComponent<Rigidbody2D>();
        myBoxCollider = GetComponent<BoxCollider2D>();
    }


    void Update()
    {
        if (IsFacingRight())
        {
            myRigibody.velocity = new Vector2(moveSpeed, 0f);
        }
        else
        {
            myRigibody.velocity = new Vector2(-moveSpeed, 0f);
        }
    }
    private bool IsFacingRight()
    {
        return transform.localScale.x > Mathf.Epsilon;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (!other.CompareTag("Player"))
        {
            transform.localScale = new Vector2(-(Mathf.Sign(myRigibody.velocity.x)), transform.localScale.y);
        }
    }
}