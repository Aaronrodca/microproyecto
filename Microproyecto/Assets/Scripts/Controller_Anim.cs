using System.Collections;
using System.Collections.Generic;
using TarodevController;
using UnityEngine;
using static Unity.VisualScripting.Member;

public class Controller_Anim : MonoBehaviour
{
    private Animator _anim;
    private PlayerController _playerController;
    public AudioSource _sourceMove;
    public AudioSource _sourceJump;
    public AudioClip[] _clip;

    private bool soundPlaying = false;

    private void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _playerController = GetComponentInParent<PlayerController>();
        _playerController.Jumped += OnJumped;
    }

    private void OnDestroy()
    {
        _playerController.Jumped -= OnJumped;
    }

    private void OnJumped()
    {
        _sourceJump.PlayOneShot(_clip[1]);
    }

    void Update()
    {
        // Movimiento y sonido
        if (Input.GetAxisRaw("Horizontal") != 0 && _playerController._grounded)
        {
            if (!soundPlaying)
            {
                _sourceMove.loop = true;
                _sourceMove.Play();
                soundPlaying = true;
            }
        }
        else
        {
            if (soundPlaying)
            {
                _sourceMove.Stop();
                soundPlaying = false;
            }
        }

        // Animaciones
        if (Input.GetAxisRaw("Horizontal") != 0 && _playerController._grounded)
        {
            _anim.SetBool("Move", true);
        }
        else
        {
            _anim.SetBool("Move", false);
        }

        if (!_playerController._grounded)
        {
            _anim.SetBool("Jump", true);
        }

        if (Input.GetButton("Inverse") && !_playerController._grounded && BarraVida.muerte == false && MenuControlJuego.isPause == false)
        {
            _anim.SetTrigger("Giro");
        }

        // Reseteo de las variables cuando el jugador est� en el suelo
        if (_playerController._grounded)
        {
            _anim.SetBool("Jump", false);
        }
    }
}
