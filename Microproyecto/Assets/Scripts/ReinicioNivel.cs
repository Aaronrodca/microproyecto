using System;
using System.Collections;
using System.Collections.Generic;
using TarodevController;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReinicioNivel : MonoBehaviour
{
    private ParticleSystem pSangre;
    private GameObject _gameObject;
    private SpriteRenderer _anim;

    private void Awake()
    {
        _gameObject = GameObject.FindGameObjectWithTag("Player");

        pSangre = GameObject.FindGameObjectWithTag("Particle").GetComponent<ParticleSystem>();
        _anim = GameObject.FindGameObjectWithTag("SpritePlayer").GetComponent< SpriteRenderer>();
        pSangre.Stop();
    }
    public void Reiniciar()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ParticulasSangre()
    {
        _anim.enabled = false;
        pSangre.Play();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Trampa"))
        {
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            /*Debug.Log("�aaaa");
            _gameObject.SetActive(false);
            _gameObject.transform.position = Vector3.zero;
            _stats.NormalGravity = true;
            _rb.velocity = Vector2.zero;
            _gameObject.SetActive(true);*/
        }
    }
}
