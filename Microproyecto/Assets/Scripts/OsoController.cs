using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OsoController : MonoBehaviour
{
    public GameObject _personaje;
    public bool estaOsoAtacando = false;
    public float fuerzaOso = 60;
    public float direccion = 0f;
    public LayerMask groundLayer;
    public BoxCollider2D _colDetectDer;
    public BoxCollider2D _colDetectIzq;

    private Rigidbody2D _rb;
    private SpriteRenderer _render;
    private float _distanceDelta = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _render = GetComponent<SpriteRenderer>();
        direccion = 0;
        estaOsoAtacando = false;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // Calcula la distancia del Raycast basada en el tama�o del collider y el delta adicional
        float distance = GetComponent<Collider2D>().bounds.extents.y + _distanceDelta;

        // Realiza el Raycast desde la posici�n del objeto hacia abajo
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, distance, groundLayer);

        // Verifica si el Raycast colisiona con algo en la capa del suelo
        if (hit.collider == null)
        {
            //Debug.Log("Colisi�n con el suelo detectada.");
            _rb.AddForce(new Vector2(0, -100), ForceMode2D.Force);
            //_colDetectDer.enabled = false;
            //_colDetectIzq.enabled = false;
        }
        else
        {
            estaOsoAtacando = false;
            //_colDetectDer.enabled = true;
            //_colDetectIzq.enabled = true;
        }

        if (_rb.velocity.x == 0)
        {
            //direccion = 0;
            estaOsoAtacando = false;
        }

        if (direccion == 2 && !estaOsoAtacando)
        {
            AtaqueDerecha();
            direccion = 0;
        }
        else if (direccion == 1 && !estaOsoAtacando)
        {
            AtaqueIzquierda();
            direccion = 0;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        /*if (other.CompareTag("Trampa"))
        {
            gameObject.SetActive(false);
        }
        /*if (other.gameObject == _personaje && !estaOsoAtacando)
        {
            estaOsoAtacando = true;
            AtaqueOso();
        }*/
    }
    public void AtaqueDerecha()
    {
        _rb.AddForce((new Vector2(1, 1).normalized) * fuerzaOso, ForceMode2D.Impulse);
        estaOsoAtacando = true;
    }
    public void AtaqueIzquierda()
    {
        _rb.AddForce((new Vector2(-1, 1).normalized) * fuerzaOso, ForceMode2D.Impulse);
        estaOsoAtacando = true;
    }
    public void AtaqueOso()
    {
        //_oso_rb.velocity = new Vector2(-2,0);
        //_rb.AddForce((new Vector2(-1, 1).normalized)*fuerzaOso, ForceMode2D.Impulse);
        //estaOsoAtacando = false;
    }
}
