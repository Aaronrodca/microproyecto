using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuControlJuego : MonoBehaviour
{
    public static bool isPause = false;
    public GameObject menuPausa;

    // Start is called before the first frame update
    void Start()
    {
        menuPausa.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel") && isPause == false)
        {
            isPause = true;
            menuPausa.SetActive(true);
            Time.timeScale = 0;
        }
        else if (Input.GetButtonDown("Cancel") && isPause == true)
        {
            btnContinuar();
        }
    }

    public void btnContinuar()
    {
        isPause = false;
        menuPausa.SetActive(false);
        Time.timeScale = 1.0f;
    }

    public void EscenaMenuInicio()
    {
        SceneManager.LoadScene("0.Menu_Inicio");
        Time.timeScale = 1.0f;
    }

    public void LoadNextLevel()
    {
        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        int nextSceneIndex = currentSceneIndex + 1;

        // Comprueba si el siguiente �ndice de escena es v�lido
        if (nextSceneIndex < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(nextSceneIndex);
        }
        else
        {
            Debug.Log("No hay m�s niveles para cargar.");
            // Aqu� puedes reiniciar el juego o ir a una pantalla de fin de juego
            // SceneManager.LoadScene(0); // Ejemplo de reiniciar el juego
        }
    }
}
