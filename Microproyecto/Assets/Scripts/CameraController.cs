using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform target; // Referencia al transform del jugador
    public float smoothSpeedX = 10f; // Velocidad de suavizado (lerp) en el eje X
    public float smoothSpeedY = 20f; // Velocidad de suavizado (lerp) en el eje Y
    public float maxXSpeed = 100f; // Velocidad m�xima de la c�mara en el eje X
    public float maxYSpeed = 100f; // Velocidad m�xima de la c�mara en el eje Y
    public float distancia_diferencia = 0.8f;

    void LateUpdate()
    {
        // Si el jugador no est� asignado, no hay nada que seguir
        if (target == null)
            return;

        // Calcular la posici�n objetivo de la c�mara en X
        Vector3 desiredPositionX = new Vector3(target.position.x, transform.position.y, transform.position.z);

        // Calcular la posici�n objetivo de la c�mara en Y
        Vector3 desiredPositionY = new Vector3(transform.position.x, target.position.y + distancia_diferencia, transform.position.z);

        // Calcular la distancia que la c�mara deber�a moverse en este frame en X
        float distanceToMoveX = Mathf.Min(Mathf.Abs(desiredPositionX.x - transform.position.x), maxXSpeed);

        // Calcular la distancia que la c�mara deber�a moverse en este frame en Y
        float distanceToMoveY = Mathf.Min(Mathf.Abs(desiredPositionY.y - transform.position.y), maxYSpeed);

        // Calcular la posici�n suavizada de la c�mara en X usando lerp limitado por la distancia a mover
        Vector3 smoothedPositionX = Vector3.Lerp(transform.position, desiredPositionX, smoothSpeedX * Time.deltaTime);

        // Calcular la posici�n suavizada de la c�mara en Y usando lerp limitado por la distancia a mover
        Vector3 smoothedPositionY = Vector3.Lerp(transform.position, desiredPositionY, smoothSpeedY * Time.deltaTime);

        // Actualizar la posici�n de la c�mara
        transform.position = new Vector3(smoothedPositionX.x, smoothedPositionY.y, transform.position.z);
    }
}
