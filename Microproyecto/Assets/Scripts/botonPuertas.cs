using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class botonPuertas : MonoBehaviour
{
    public Animator _puerta;
    public GameObject[] _interaccion;
    private float count = 0f;
    public Sprite _btnPress;
    public Sprite _btnNoPress;
    private SpriteRenderer _btn;
    // Start is called before the first frame update
    void Start()
    {
        _btn = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        
            
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        /*if (other.gameObject == _interaccion)
        {
            _puerta.SetBool("abrir", true);
        }*/
        foreach (var item in _interaccion)
        {
            count++;
        }
        if (count > 0)
        {
            _puerta.SetBool("abrir", true);
            _btn.sprite = _btnPress;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (_puerta != null)
        {
            {
                foreach (var item in _interaccion)
                {
                    count--;
                }
                if (count <= 0)
                {
                    _puerta.SetBool("abrir", false);
                    _btn.sprite = _btnNoPress;
                }
            }
        }
    }
}
