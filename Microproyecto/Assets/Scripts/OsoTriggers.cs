using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OsoTriggers : MonoBehaviour
{
    //public OsoController osoController;
    public bool isRightTrigger;
    public GameObject _player;
    public OsoController _osoController;

    private SpriteRenderer _render;
    private void Start()
    {
        _render = GetComponent<SpriteRenderer>();
        //_render.enabled = false;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == _player)
        {
            if (isRightTrigger && _osoController.direccion == 0)
            {
                //Debug.Log("Derecha");
                _osoController.direccion = 1;
                //osoController.AtaqueDerecha();
            }
            else if (!isRightTrigger && _osoController.direccion == 0)
            {
                //Debug.Log("Iquierda");
                _osoController.direccion = 2;
                //osoController.AtaqueIzquierda();
            }
        }
    }
}
