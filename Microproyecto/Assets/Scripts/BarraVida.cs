using System.Collections;
using System.Collections.Generic;
using TarodevController;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class BarraVida : MonoBehaviour
{
    private Animator _anim;
    private Image barraVida;
    public float vidaActual;
    public float vidaMaxima;
    private float _time;
    public static bool muerte;

    private void Awake()
    {
        muerte = false;
        _anim = GameObject.FindGameObjectWithTag("SpritePlayer").GetComponent<Animator>();
        barraVida = GameObject.FindGameObjectWithTag("Barra Vida").GetComponent<Image>();
    }

    void Update()
    {
        _time += Time.deltaTime;
        barraVida.fillAmount = vidaActual / vidaMaxima;

        // Cuando la vida es 0 se reinicia el nivel
        if (vidaActual <= 0)
        {
            muerte = true;
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            gameObject.GetComponent<PlayerController>().enabled = false;
            _anim.SetTrigger("Muerte");
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        // Cuando se presiona el boton invertir se quitara vida
        if (Input.GetButtonDown("Inverse") && MenuControlJuego.isPause == false)
        {
            vidaActual -= 12;
        }

        // Si la vida es mayor que 0 entonces cada x segundos ir� incrementando
        if ((vidaActual > 0 && vidaActual < vidaMaxima) && !GanarNivel.partidaGanada)
        {
            vidaActual += Time.deltaTime*5;
        }
    }

    public void Reiniciar()
    {
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Trampa"))
        {
            vidaActual -= 120;
            //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        GameObject otherGameObject = other.gameObject;

        if (otherGameObject.CompareTag("Trampa"))
        {
            vidaActual -= 120;
        }
    }
}
