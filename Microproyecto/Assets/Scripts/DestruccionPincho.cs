using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruccionPincho : MonoBehaviour
{
    private Animator _anim;
    private Collider2D _col;
    private Rigidbody2D _rb;
    private PolygonCollider2D _colDa�o;
    // Start is called before the first frame update
    void Awake()
    {
        _anim = GetComponent<Animator>();
        _col = GetComponent<Collider2D>();
        _colDa�o = GetComponentInChildren<PolygonCollider2D>();
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (_anim != null)
        {
            _anim.SetBool("activo", true);
            _colDa�o.enabled = false;
            _rb.simulated = false;
        }
    }
}
