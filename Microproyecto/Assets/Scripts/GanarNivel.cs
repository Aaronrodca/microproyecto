using System.Collections;
using System.Collections.Generic;
using TarodevController;
using Unity.Burst.CompilerServices;
using UnityEngine;

public class GanarNivel : MonoBehaviour
{
    public int puntaje;
    public SpriteRenderer _spriteEstrella;
    public UnityEngine.UI.Text _txtTiempoReferencia;
    public UnityEngine.UI.Text[] _txtTiempo;
    public GameObject _meta;
    public GameObject _pantallaNivelCompleto;
    public GameObject _spriteJugador;
    public static bool partidaGanada = false;

    public SettingWorld _world;
    // Start is called before the first frame update
    void Awake()
    {
        _meta.GetComponent<Renderer>().enabled = false;
    }

    void Start()
    {
        _pantallaNivelCompleto.SetActive(false);
        puntaje = 3;
        ChangePoint(puntaje);
    }

    // Update is called once per frame
    void Update()
    {
        if (Cronometro._counter >= 0 && Cronometro._counter < _world.tresEstrellas)
        {
            puntaje = 3;
            ChangePoint(puntaje);
        }
        else if (Cronometro._counter >= _world.tresEstrellas && Cronometro._counter < _world.dosEstrellas)
        {
            puntaje = 2;
            ChangePoint(puntaje);
        }
        else if (Cronometro._counter >= _world.dosEstrellas && Cronometro._counter < _world.unaEstrella)
        {
            puntaje = 1;
            ChangePoint(puntaje);
        }
        else if (Cronometro._counter >= _world.unaEstrella)
        {
            puntaje = 0;
            ChangePoint(puntaje);
        }
    }
    void txtTiempoPantallaSuperacion()
    {
        foreach (UnityEngine.UI.Text txt in _txtTiempo)
        {
            txt.text = _txtTiempoReferencia.text;
        }
    }

    void ChangePoint(float height)
    {
        // Obtener el tama�o actual del sprite
        Vector2 size = _spriteEstrella.size;

        // Modificar la altura del sprite
        size.x = height;

        // Asignar el nuevo tama�o al SpriteRenderer
        _spriteEstrella.size = size;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == _meta)
        {
            partidaGanada = true;
            _pantallaNivelCompleto.SetActive(true);
            gameObject.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
            gameObject.GetComponent<PlayerController>().enabled = false;
            _spriteJugador.GetComponent<Animator>().enabled = false;
            txtTiempoPantallaSuperacion();
        }
    }
}
