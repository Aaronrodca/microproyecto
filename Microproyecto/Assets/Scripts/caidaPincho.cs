using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class caidaPincho : MonoBehaviour
{
    public Rigidbody2D _rbPincho;
    public int isActiveGravity = 0;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _rbPincho.AddForce(new Vector2(0, -10* isActiveGravity), ForceMode2D.Impulse);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            isActiveGravity = 1;
        }
    }
}
