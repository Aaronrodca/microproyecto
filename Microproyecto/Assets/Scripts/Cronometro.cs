using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cronometro : MonoBehaviour
{
    public UnityEngine.UI.Text[] _txtTiempo;
    public static float _counter;
    // Start is called before the first frame update
    void Start()
    {
        _counter = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        if(MenuControlJuego.isPause == false && !GanarNivel.partidaGanada)
        {
            _counter += Time.deltaTime;
            UpdateTimerDisplay();
        }
    }

    void UpdateTimerDisplay()
    {
        // Calcular minutos, segundos y milisegundos
        int minutos = Mathf.FloorToInt(_counter / 60);
        int segundos = Mathf.FloorToInt(_counter % 60);
        int milisegundos = Mathf.FloorToInt((_counter % 1) * 100);

        // Formatear en una cadena "00:00:00"
        string formattedTime = string.Format("{0:00}.{1:00}.{2:00}", minutos, segundos, milisegundos);

        // Actualizar cada objeto de texto en el array
        foreach (UnityEngine.UI.Text txt in _txtTiempo)
        {
            txt.text = "Tiempo: " + formattedTime;
        }
    }

}
