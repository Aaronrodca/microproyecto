using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoReiniciar : MonoBehaviour
{
    public AudioSource audioSource;

    private static NoReiniciar instance;

    private void Awake()
    {
        // Verificar si ya existe una instancia de NoReiniciar
        if (instance == null)
        {
            // Si no existe, establecer esta instancia como la instancia �nica
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            // Si ya existe otra instancia, destruir esta para mantener solo una
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
