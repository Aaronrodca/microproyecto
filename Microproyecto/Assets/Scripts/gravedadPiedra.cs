using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gravedadPiedra : MonoBehaviour
{
    private Rigidbody2D _rb;
    public float velocity = 10f;
    // Start is called before the first frame update
    void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //_rb.AddForce(new Vector2(0, -velocity), ForceMode2D.Impulse);
        _rb.velocity = new Vector2(0, -velocity);
    }
}
